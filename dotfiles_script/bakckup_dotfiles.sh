#!/bin/bash
mkdir -p ~/.config
mkdir -p ~/.config/alacritty

cp ~/.config/starship.toml ../dotfiles/.config/starship.toml
cp ~/.config/alacritty/alacritty.yml ../dotfiles/.config/alacritty/alacritty.yml
cp ~/.bashrc ../dotfiles/.bashrc
cp ~/.zshrc ../dotfiles/.zshrc
