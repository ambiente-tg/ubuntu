#!/bin/bash
mkdir -p ~/.config
mkdir -p ~/.config/alacritty

cp ../dotfiles/.config/starship.toml ~/.config/starship.toml
cp ../dotfiles/.config/alacritty/alacritty.yml ~/.config/alacritty/alacritty.yml
cp ../dotfiles/.bashrc ~/.bashrc
cp ../dotfiles/.zshrc ~/.zshrc
