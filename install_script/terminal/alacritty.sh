#!/bin/bash

sudo apt-get install -y cmake pkg-config libfreetype6-dev libfontconfig1-dev libxcb-xfixes0-dev python3

# Download Alacritty
git clone https://github.com/alacritty/alacritty ~/Downloads/alacritty
cd ~/Downloads/alacritty

source $HOME/.cargo/env

cargo build --release

sudo cp target/release/alacritty /usr/local/bin;
sudo cp extra/logo/alacritty-term.svg /usr/share/pixmaps/Alacritty.svg;
sudo desktop-file-install extra/linux/Alacritty.desktop;
sudo update-desktop-database;

sudo tic -xe alacritty,alacritty-direct extra/alacritty.info

cp alacritty.yml ~/.alacritty.yml

mkdir -p ${ZDOTDIR:-~}/.zsh_functions
echo 'fpath+=${ZDOTDIR:-~}/.zsh_functions' >> ${ZDOTDIR:-~}/.zshrc

mkdir -p ~/.bash_completion
cp extra/completions/alacritty.bash ~/.bash_completion/alacritty
echo "source ~/.bash_completion/alacritty" >> ~/.bashrc

gsettings set org.gnome.desktop.default-applications.terminal exec 'alacritty'

cd ..
rm -rfd alacritty


