#!/bin/bash

# Install Latest Version
curl -fsSL https://starship.rs/install.sh | bash

# ~/.bashrc
echo '' &>>~/.bashrc
echo '## Starship' &>>~/.bashrc
echo 'eval "$(starship init bash)"' &>>~/.bashrc

#~/.zshrc
echo '' &>>~/.zshrc
echo '## Starship' &>>~/.zshrc
echo 'eval "$(starship init bash)"' &>>~/.zshrc